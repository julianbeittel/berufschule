---
tags: ["Berufschule", "BGP"]
---

# Phasenmodell der Eskalation nach Glasl
## Generele Information
- Estellt von Friedrich Glasl im Jahr 1965
- Ziel: Konflikte zu Analysieren 

## Die Phasen
Das Phasenmodell besteht aus 3 Hauptphasen die jewilige in 3 Unterphasen augeteilt werden.

- WIN-WIN
    - Verhärtung
    - Polarisation & Debate
    - Taten statt Worte
- WIN-LOSE
    - Sorgen um Image und Koalition
    - Gesichtsverlust
    - Drohstratigeien
- LOSE-LOSE
    - Begrenzte Vernichtungsanschläge
    - Zersplittering
    - Germeinsam in den Abgrund

### WIN-WIN
In dieser Phase kann man noch Rauskommen ohne das irgeneine Seite einen Verlust einsehen muss oder fühlt.

**Verhärtung**
- Gelegentliches Aufprallen von Meinungen
- Alles im Alltag, nichts als Konflikt erkannt
- Leute *Verhärten* sich auf ihre Meinungen

**Debatte, Polemik**
- Strategien werden gesucht
- Meinungsverschiedenheite führen zu Streit
- Schwarz-Weiß gedanken setzen ein

**Tatten statt Worte**
- Druck erhöt sich
- Gespräche werden abgebrochen
- Mitgefühl zum anderen verschwindet

### WIN-LOSE
In dieser Phasen ereicht man den Punkt wo ein Konfliktpartner eine Verlust erlben muss

**Koaliiton, Image**
- Recherschiert mit Bias aufs eigen Thema
    - Bestätigt das man im Recht ist
- Es handelt sich ums Gewinnen jetzt, nich ums Thema

**Gesichtsverlust**
- Die Identität von der Person angegriffen
- Vertrauen ist vollständig Verloren

**Drohstrategien**
- Man Droht die Person an, das Thema anzunehmen
- Bestandteile einer Drohung
    - Forderungen
    - Sanktionen
    - sanktionpotenzial
